@db = {}

db.Schema = {}

Talents =
  'Music': [
    'Composer'
    'Bassist'
    'Cellist'
    'Conductor'
    'DJ'
    'Drummer'
    'Guitarist'
    'Pianist'
    'Rapper'
    'Saxophonist'
    'Trumpeter'
    'Violinist'
    'Vocalist'
  ]
  'Film & Video': [
    'Actor'
    'Actress'
    'Costume Designer'
    'Costume Stylist'
    'Stuntman'
    'Videographer'
  ]
  'Photography & Arts': [
    'Cartoonist'
    'Graffiti Artist'
    'Magician'
    'Painter'
    'Photographer'
    'Sculptor'
  ]
  'Modeling': [
    'Fashion Designer'
    'Hairstylist'
    'Make up artist'
    'Model'
  ]
  'Dance': [
    'Backup Dancer'
    'Choreographer'
    'Lead Dancer'
  ]
Barters = [
  'Time'
  'Demo'
  'Shoutout'
  'Gigs/Tickets'
  'Video'
  'Props'
  'Clothes'
  'Accessories'
  'Prints'
  'Studio Space'
]
db.Schema.Submission = new SimpleSchema(
  projectId:
    type: String
    optional: true
  owner:
    type: String
    optional: true
    autoform: type: 'hidden'
  format:
    type: String
    allowedValues: [
      'Audio'
      'Video'
      'Image'
    ]
    autoform: label: 'Choose media type'
  description:
    type: String
    optional: true
    autoform: type: 'textarea'
  url:
    type: String
    optional: true
  createdAt:
    type: Date
    defaultValue: new Date
  liked:
    type: Boolean
    optional: true)
db.Submissions = new (Mongo.Collection)('submissions')
db.Submissions.attachSchema db.Schema.Submission
db.Submissions.allow insert: ->
  true
db.Schema.Project = new SimpleSchema(
  name:
    type: String
    min: 1
  submissionDate:
    type: Date
    optional: true
    defaultValue: new Date
  projectType:
    type: String
    allowedValues: [
      'Barter'
      'Collaborate'
      'Bounty'
    ]
    autoform: type: 'select'
  location:
    type: String
    optional: false
  bounty:
    type: Number
    optional: true
  description:
    type: String
    optional: true
    autoform: type: 'textarea'
  owner:
    type: String
    optional: true
    autoform: type: 'hidden'
  trade:
    type: String
    allowedValues: Barters
    optional: true
  tradeFor:
    type: String
    allowedValues: Barters
    optional: true
  examples:
    type: [ String ]
    optional: true
    regEx: SimpleSchema.RegEx.Id
  submissions:
    type: Array
    optional: true
  'submissions.$': type: Object
  'submissions.$.user':
    type: String
    regEx: SimpleSchema.RegEx.Id
  'submissions.$.submissions':
    type: [ String ]
    optional: true
    regEx: SimpleSchema.RegEx.Id
  'submissions.$.talent':
    type: String
    label: 'Talent'
    allowedValues: Object.keys(Talents)
  'submissions.$.subtalent':
    type: String
    label: 'Sub Talent'
    autoform: options: ->
      talent = AutoForm.getFieldValue(@name.replace('.subtalent', '.talent'))
      _.map Talents[talent], (t) ->
        {
          label: t
          value: t
        }
  collaborators:
    type: Array
    optional: true
  'collaborators.$': type: Object
  'collaborators.$.talent':
    type: String
    label: 'Talent'
    allowedValues: Object.keys(Talents)
  'collaborators.$.subtalent':
    type: String
    label: 'Sub Talent'
    autoform: options: ->
      talent = AutoForm.getFieldValue(@name.replace('.subtalent', '.talent'))
      _.map Talents[talent], (t) ->
        {
          label: t
          value: t
        }
  'collaborators.$.quantity': type: Number
  'collaborators.$.bounty':
    type: Number
    optional: true)
db.Projects = new (Mongo.Collection)('projects')
db.Projects.attachSchema db.Schema.Project
db.Projects.allow insert: ->
  true
db.Schema.UserProfile = new SimpleSchema(
  firstName:
    type: String
    label: 'First Name'
  lastName:
    type: String
    label: 'Last Name'
  talent:
    type: String
    label: 'Talent'
    allowedValues: Object.keys(Talents)
  subtalent:
    type: String
    label: 'Sub Talent'
    autoform: options: ->
      talent = AutoForm.getFieldValue(@name.replace('.subtalent', '.talent'))
      _.map Talents[talent], (t) ->
        {
          label: t
          value: t
        }
  wantsPushNotifications:
    type: Boolean
    label: 'Push Notifications'
  wantsEmailNotifications:
    type: Boolean
    label: 'Email')
db.Schema.User = new SimpleSchema(
  username:
    type: String
    optional: true
  emails:
    type: Array
    optional: false
  'emails.$': type: Object
  'emails.$.address':
    type: String
    label: 'Email Address'
    regEx: SimpleSchema.RegEx.Email
  'emails.$.verified': type: Boolean
  createdAt: type: Date
  profile:
    type: db.Schema.UserProfile
    optional: true
  services:
    type: Object
    optional: true
    blackbox: true)
Meteor.users.attachSchema db.Schema.User
