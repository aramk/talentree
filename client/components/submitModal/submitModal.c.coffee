nameToKeyValue = (name) ->
  {
    key: name
    name: name
  }

Template.submitModal.created = ->
  @selectedSubmissions = new ReactiveVar([])
  if @data
    firstCollaborator = @data.collaborators[0]
    Session.set 'selectedTalent', Session.get('selectedTalent') or firstCollaborator.talent
    Session.set 'selectedSubtalent', Session.get('selectedSubtalent') or firstCollaborator.subtalent
  $('.ui.modal').modal()
  return

Template.submitModal.helpers
  submissions: ->
    db.Submissions.find {}
  isModalActive: ->
    Session.get('activeModal') == 'submit'
  isSelectedTalent: (talent) ->
    Session.get('selectedTalent') == talent
  isSelectedSubtalent: (subtalent) ->
    Session.get('selectedSubtalent') == subtalent
  talentOptions: ->
    collaborators = @collaborators
    _.chain(collaborators).pluck('talent').uniq().map(nameToKeyValue).value()
  subtalentOptions: ->
    collaborators = @collaborators
    selectedTalent = Session.get('selectedTalent')
    _.chain(collaborators).filter((c) ->
      c.talent == selectedTalent
    ).pluck('subtalent').uniq().map(nameToKeyValue).value()
Template.submitModal.events
  'click button#close': (e) ->
    Session.set 'selectedTalent', null
    Session.set 'selectedSubtalent', null
    Session.set 'activeModal', null
    return
  'click button#submit': (e, template) ->
    db.Projects.update { _id: template.data._id }, $push: submissions:
      user: Meteor.user()._id
      submissions: template.selectedSubmissions.get()
      talent: Session.get('selectedTalent')
      subtalent: Session.get('selectedSubtalent')
    Session.set 'selectedTalent', null
    Session.set 'selectedSubtalent', null
    Session.set 'activeModal', null
    return
  'change .talent-selector': (e, template) ->
    Session.set 'selectedTalent', e.target.value
    return
  'change .subtalent-selector': (e, template) ->
    Session.set 'selectedSubtalent', e.target.value
    return
  'click .card': (e, template) ->
    submissionID = $($(e.delegateTarget).find('.card')[0]).data('submission-id')
    template.selectedSubmissions.set template.selectedSubmissions.get().concat(submissionID)
    return
