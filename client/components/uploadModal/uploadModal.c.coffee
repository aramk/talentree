Template.uploadModal.created = ->
  $('.ui.modal').modal()
  return

Template.uploadModal.helpers isModalActive: ->
  Session.get('activeModal') == 'upload'
AutoForm.hooks uploadModalForm:
  before: insert: (doc, template) ->
    imageFile = ($('#imageFile') or [])[0]
    self = this
    if imageFile
      files = imageFile.files
      S3.upload {
        files: files
        path: 'submissions'
      }, (e, r) ->
        doc = _.extend(doc, url: r.secure_url)
        Session.set 'activeModal', null
        self.result doc
        return
    else
      Session.set 'activeModal', null
      self.result doc
    return
  onSuccess: (formType, doc) ->
    newList = (Session.get('newProjectExamples') or []).concat(doc)
    Session.set 'newProjectExamples', newList
    return
Template.uploadModal.events 'click button': (e) ->
  Session.set 'activeModal', null
  return
