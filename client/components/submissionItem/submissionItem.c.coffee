Template.submissionItem.helpers
  isFormat: (format) ->
    Template.instance().data.format == format
  getAudioSource: ->
    url = Template.instance().data.url or ''
    if url
      'http://w.soundcloud.com/player?url=' + Template.instance().data.url
    else
      null
  getVideoSource: ->
    url = Template.instance().data.url or ''
    if Util.YouTubeGetID(url)
      'http://www.youtube.com/embed/' + Util.YouTubeGetID(url)
    else if Util.VimeoGetId(url)
      'http://player.vimeo.com/video/' + Util.VimeoGetId(url)
    else
      null
  getImageUrl: ->
    Template.instance().data.url
