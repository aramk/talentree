@Util = {}

Util.getSubmissionsInList = (list) ->
  db.Submissions.find _id: $in: list or []

#https://gist.github.com/takien/4077195

Util.YouTubeGetID = (url) ->
  ID = ''
  url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/)
  if url[2] != undefined
    ID = url[2].split(/[^0-9a-z_\-]/i)
    return ID[0]
  return

Util.VimeoGetId = (url) ->
  vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/
  match = url.match(vimeo_Reg)
  if match
    return match[3]
  return
