Router.route '/user/projects/', ->
  @layout 'layout'
  @render 'userProjects'
  return

Template.userProjects.created = ->
  @selectedTab = new ReactiveVar('__ALL__')
  return

Template.userProjects.helpers
  tabs: ->
    selectedTab = Template.instance().selectedTab.get()
    [
      {
        value: '__ALL__'
        label: 'All'
        classes: if selectedTab == '__ALL__' then 'active' else ''
      }
      {
        value: 'Barter'
        label: 'Barter'
        classes: if selectedTab == 'Barter' then 'active' else ''
      }
      {
        value: 'Collaborate'
        label: 'Collaborate'
        classes: if selectedTab == 'Collaborate' then 'active' else ''
      }
      {
        value: 'Bounty'
        label: 'Bounty'
        classes: if selectedTab == 'Bounty' then 'active' else ''
      }
    ]
  filteredProjects: ->
    selectedTab = Template.instance().selectedTab.get()
    filter = if selectedTab == '__ALL__' then {} else projectType: selectedTab
    db.Projects.find filter
  noProjects: ->
    false
Template.userProjects.events 'click a': (e) ->
  Template.instance().selectedTab.set $(e.target).data('tab')
  return
