Router.route '/project/new/', ->
  @layout 'layout'
  @render 'projectNew'
  return
Template.projectNew.onRendered ->
  Session.set 'newProjectExamples', []
  return
AutoForm.hooks newProjectForm:
  before: insert: (doc, template) ->
    doc.examples = Session.get('newProjectExamples')
    doc
  onSuccess: (formType, doc) ->
  onError: ->
    console.log arguments
    return
Template.projectNew.helpers examples: ->
  Util.getSubmissionsInList Session.get('newProjectExamples')
Template.projectNew.events 'click #upload': (e) ->
  Session.set 'activeModal', 'upload'
  return
