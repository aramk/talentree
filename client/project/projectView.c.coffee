Router.route '/project/:id/', ->
  @layout 'layout'
  @render 'projectView', data: ->
    db.Projects.findOne @params.id
  return

Template.projectView.created = ->
  @selectedTab = new ReactiveVar('Details')
  # var fullName = Meteor.user().firstName + ' ' + Meteor.user().lastName;
  # Session.set('chapp-username', fullName);
  Session.set 'chapp-docid', @_id
  return

Template.projectView.helpers
  isSelectedTab: (tab) ->
    selectedTab = Template.instance().selectedTab.get()
    tab == selectedTab
  exampleItems: ->
    examples = @examples
    Util.getSubmissionsInList examples
  userSubmissions: ->
    submissions = @submissions
    _.map submissions, (s) ->
      user = Meteor.users.findOne(s.user)
      {
        talent: s.talent
        subtalent: s.subtalent
        userName: user.firstName + ' ' + user.lastName
        submissions: Util.getSubmissionsInList(s.submissions)
      }
  tabs: ->
    selectedTab = Template.instance().selectedTab.get()
    [
      {
        value: 'Details'
        label: 'Details'
        classes: if selectedTab == 'Details' then 'active' else ''
      }
      {
        value: 'Submissions'
        label: 'Submissions'
        classes: if selectedTab == 'Submissions' then 'active' else ''
      }
      {
        value: 'Comments'
        label: 'Comments'
        classes: if selectedTab == 'Comments' then 'active' else ''
      }
    ]
  getIconType: ->
    ICON_TYPES = 
      'Barter': 'comments'
      'Collaborate': 'idea'
      'Bounty': 'dollar'
    ICON_TYPES[@projectType]
Template.projectView.events
  'click a.item': (e, template) ->
    template.selectedTab.set $(e.target).data('tab')
    return
  'click li.collaborator': (e) ->
    Session.set 'activeModal', 'submit'
    Session.set 'selectedTalent', $(e.target).data('talent')
    Session.set 'selectedSubtalent', $(e.target).data('subtalent')
    return
  'click .participate-button': (e) ->
    Session.set 'activeModal', 'submit'
    return
