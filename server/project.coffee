Meteor.methods
  'insertProject': (project) ->
    console.log 'Insert project called: ' + JSON.stringify(project)
    # remove extraneous fields
    project = _.pick(project, _.keys(db.Schemas.Project.schema()))
    if project.bounty
      bounty = parseInt(project.bounty)
      if bounty and _.isNumber(bounty) and !_.isNaN(bounty)
        project.bounty = bounty
      else
        console.log 'Bounty is not a number: ' + project.bounty
        throw new (Meteor.Error)('Bad input', 'Bounty is not a number.')
    check project, db.Schemas.Project
    db.Projects.insert project
    return
  'insertSubmission': (submission) ->
    console.log 'Insert submission ' + JSON.stringify(submission)
    submission = _.pick(submission, _.keys(db.Schemas.Submission.schema()))
    submission.createdAt = new Date
    check submission, db.Schemas.Submission
    submissionId = db.Submissions.insert(submission)
    db.Projects.update { _id: submission.projectId }, $push: submissions: submissionId
    return
