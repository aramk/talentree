S3.config =
  key: Meteor.settings.awsKey
  secret: Meteor.settings.awsSecret
  bucket: Meteor.settings.s3Bucket
  region: Meteor.settings.awsRegion
